//
//  FavUniViewController.swift
//  ParseLogin
//
//  Created by Tyler Barnes on 3/16/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

import UIKit
import Parse

class FavUniViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet var FavUniTableView: UITableView!
    @IBOutlet var Logout: UIBarButtonItem!
    
    public var Users: [PFObject]? = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(PFUser.current() == nil){
            print("Error: User LoggedOut")
        }
        
        FavUniTableView.register(UINib(nibName: "UniTableViewCell", bundle: nil), forCellReuseIdentifier: "myCell")
        
        loadInfo()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Account(_ sender: UIBarButtonItem) {
        
        performSegue(withIdentifier: "Account", sender: Any?.self)
    }

    @IBAction func Logout(_ sender: UIBarButtonItem) {
        
        PFUser.logOutInBackground { (error) in
            if !(error != nil) {
                print("Logout Success")
            }
        }
        
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateInitialViewController()
        self.view.window?.rootViewController = vc
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Users!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! UniTableViewCell
        
        let user = Users?[indexPath.row] 
        
        cell.User.text = user?.value(forKey: "username") as! String?
        cell.Uni.text = user?.value(forKey: "FavUni") as! String?
        
        return cell
    }
    
    func loadInfo(){
        
        let query = PFUser.query()
        query?.findObjectsInBackground(block: { (users, error) in
            self.Users = users
            
            self.FavUniTableView.reloadData()
        })

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
