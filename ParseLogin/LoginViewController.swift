//
//  LoginViewController.swift
//  ParseLogin
//
//  Created by Tyler Barnes on 3/16/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

import UIKit
import Parse

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var Username: UITextField!
    @IBOutlet var Password: UITextField!
    @IBOutlet var Login: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        Username.delegate = self
        Password.delegate = self
        
        if (PFUser.current() != nil) {
//            LoggedIn(user: PFUser.current()!)
            print("Error: User LoggedIn")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Login(_ sender: UIButton) {
        
        if((Username.text != "") && (Password.text != "")){
            LoggingIn(sender: sender)
        }
        else{
            performSegue(withIdentifier: "Signup", sender: sender)
        }
        
    }
    
    func LoggingIn (sender: Any?) {
        
        PFUser.logInWithUsername(inBackground: Username.text!, password: Password.text!) { (User, error) in
            
            if error == nil {
                print("Login Success")
                self.performSegue(withIdentifier: "Login", sender: sender)
            }
            else {
                let alert = UIAlertController(title: "Login Failed", message: "Invalid Username/Password", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
                
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
//    func LoggedIn(user: PFUser) {
//        self.dismiss(animated: false, completion: nil)
//        let storyboard = UIStoryboard(name: "User", bundle: nil)
//        let vc = storyboard.instantiateInitialViewController()
//        self.
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        Username.text = ""
        Password.text = ""
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch : UITouch = touches.first!
        if(!(touch.isMember(of: UITextField.self))){
            touch.view?.endEditing(true)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        Login.sendActions(for: .touchUpInside)
        
        textField.resignFirstResponder()
        
        return false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
