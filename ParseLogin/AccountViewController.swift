//
//  AccountViewController.swift
//  ParseLogin
//
//  Created by Tyler Barnes on 3/16/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

import UIKit
import Parse

extension String {
    var isEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
}

class AccountViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var Username: UITextField!
    @IBOutlet var Email: UITextField!
    @IBOutlet var Password: UITextField!
    @IBOutlet var FavUni: UITextField!
    
    @IBOutlet var PasswordLabel: UILabel!
    @IBOutlet var changeLater: UILabel!
    @IBOutlet var CreateSave: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        Username.delegate = self
        Email.delegate = self
        Password.delegate = self
        FavUni.delegate = self
        
        if (PFUser.current() != nil) {
            Username.text = PFUser.current()?.username
            Email.text = PFUser.current()?.email
            Password.text = PFUser.current()?.password
            FavUni.text = PFUser.current()?.value(forKey: "FavUni") as! String?
            CreateSave.title = "Save"
            PasswordLabel.text = "Change Password"
            Password.placeholder = "New Password?"
            changeLater.isHidden = true
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Cancel(_ sender: UIBarButtonItem) {
        
        dismiss(animated: true, completion:nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (PFUser.current() != nil) {
            
            if (Email.text?.isEmail)! {
                
                let user = PFUser.current()
                user?.username = Username.text
                user?.email = Email.text
                
                if(FavUni.text == ""){
                    
                    let alert = UIAlertController(title: "Why'd you delete your Favorite University?", message: "You should enter one, just for kicks", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
                    alert.addAction(ok)
                    present(alert, animated: true, completion: nil)
                    
                }
                else {
                    
                    user?["FavUni"] = FavUni.text
                    
                    user?.saveInBackground(block:  { (success, error) in
                            
                        if (error != nil){
                            let alert = UIAlertController(title: "Account Error", message: "Try Again", preferredStyle: .alert)
                            let ok = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
                            alert.addAction(ok)
                            self.present(alert, animated: true, completion: nil)
                        }
                        else {
                            self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
                            let storyboard = UIStoryboard(name: "User", bundle: nil)
                            let vc = storyboard.instantiateInitialViewController()
                            self.view.window?.rootViewController = vc
                        }
                        
                    })
                }
            }
            else {
                
                let alert = UIAlertController(title: "Username Invalid", message: "Username must be email address", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
                alert.addAction(ok)
                present(alert, animated: true, completion: nil)
                return
                
            }
        }
        else {
            
            if ((Username.text != "") && (Password.text != ""))  {
                
                if (Email.text?.isEmail)! {
                    
                    let user = PFUser()
                    user.username = Username.text
                    user.email = Email.text
                    user.password = Password.text
                    
                    if(FavUni.text == ""){
                        
                        let alert = UIAlertController(title: "No Favorite University?", message: "You should enter one, just for kicks", preferredStyle: .alert)
                        let ok = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
                        alert.addAction(ok)
                        present(alert, animated: true, completion: nil)
                        
                    }
                    else {
                        
                        user["FavUni"] = FavUni.text
                        
                        user.signUpInBackground { (success, error) in
                            PFUser.logInWithUsername(inBackground: self.Username.text!, password: self.Password.text!) { (User, error) in
                                
                                if (error != nil){
                                    let alert = UIAlertController(title: "Account Error", message: "Try Again", preferredStyle: .alert)
                                    let ok = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
                                    alert.addAction(ok)
                                    self.present(alert, animated: true, completion: nil)
                                }
                                else {
                                    self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
                                    let storyboard = UIStoryboard(name: "User", bundle: nil)
                                    let vc = storyboard.instantiateInitialViewController()
                                    self.view.window?.rootViewController = vc
                                }
                            }
                        }
                    }
                }
                else {
                    
                    let alert = UIAlertController(title: "Username Invalid", message: "Username must be email address", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
                    alert.addAction(ok)
                    present(alert, animated: true, completion: nil)
                    return
                    
                }
            }
            else {
                
                let alert = UIAlertController(title: "Username/Password Empty", message: nil, preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
                alert.addAction(ok)
                present(alert, animated: true, completion: nil)
                
            }
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch : UITouch = touches.first!
        if(!(touch.isMember(of: UITextField.self))){
            touch.view?.endEditing(true)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return false
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
