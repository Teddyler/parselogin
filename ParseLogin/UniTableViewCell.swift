//
//  UniTableViewCell.swift
//  ParseLogin
//
//  Created by Tyler Barnes on 3/17/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

import UIKit

class UniTableViewCell: UITableViewCell {

    @IBOutlet var User: UILabel!
    @IBOutlet var Uni: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
